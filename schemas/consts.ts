import { GraphQLEnumType } from "graphql";

export enum STATUS {
  success = 'SUCCESS',
  error = 'ERROR',
}
export const STATUS_MUTATION = new GraphQLEnumType({
  name: 'Status',
  values: {
    success: { value: 'SUCCESS' },
    error: { value: 'ERROR'},
  },
});
