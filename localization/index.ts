import i18n from 'i18next';
import linksNs from './en/links.json';
import ns2 from './en/ns2.json';
import { initReactI18next } from 'react-i18next';

export const resources = {
  en: {
    linksNs,
    ns2,
  },
} as const;

i18n.use(initReactI18next).init({
  lng: 'en',
  ns: ['linksNs', 'ns2'],
  resources,
});
