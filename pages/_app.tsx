import '../styles/globals.scss'
import '../localization/index';

// NEXT
import Head from "next/head";
import type { AppProps } from 'next/app'
import { useRouter } from 'next/router'

// material UI
import { CssBaseline, ThemeProvider } from '@mui/material';
import theme from "../styles/theme";

// components
import { MainLayout } from "../components/layouts/MainLayout";
import { AuthLayout } from "../components/layouts/AuthLayout";

// hooks
import { AuthProvider } from "../hooks/useAuth";

function MyApp({ Component, pageProps }: AppProps) {
  const { asPath } = useRouter();

  const isAuthPage = asPath.includes('auth');

  const Layout = isAuthPage ? AuthLayout : MainLayout;

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Head>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>
      <AuthProvider>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </AuthProvider>
    </ThemeProvider>
  );
};

export default MyApp
