import { FC, ReactElement } from 'react';
import styles from "./layout.module.scss";

import { Navigation } from "../../Navigation";

export const MainLayout: FC<{ children: ReactElement }> = ({ children }) => {
  return (
    <div className={styles.layout}>
      <header className={styles.head}>
        <h2>Logo</h2>
      </header>
      <div className={styles.nav}>
        <Navigation />
      </div>
      <main className={styles.main}>
        {children}
      </main>
      <footer className={styles.footer}>
        <h2>footer</h2>
      </footer>
    </div>
  );
};
