import { graphqlHTTP } from "express-graphql";
import schema from "../../schemas/";

export default graphqlHTTP({
  schema,
  graphiql: true,
})
