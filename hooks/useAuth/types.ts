export type JwtAccessPayload = {
  email: string;
  id: string;
};
export type JwtRefreshPayload = Omit<JwtAccessPayload, "email">;
export type Tokens = { accessToken: string; refreshToken: string; };

export type LoginParams = {
  email: string;
  password: string;
}

export interface IAuthContext {
  login: (params: LoginParams) => void,
  logout: () => void,
  user: null | JwtAccessPayload,
  isLogged: boolean
};
