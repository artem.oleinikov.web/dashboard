import { useAuth } from "../../hooks/useAuth";

import type { NextPage } from 'next'

const Chats: NextPage = () => {
  const auth = useAuth();
  return (
    <div >
      Chats
    </div>
  )
};

export default Chats;
