import { createContext } from "react";
import { IAuthContext } from "./types";

export const AuthContext = createContext<IAuthContext>({
  login() {},
  logout() {},
  user: { email: "", id: "" },
  isLogged: false
});
