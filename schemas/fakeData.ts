import { IClient } from "./client";
import { IMessage } from "./message";
import { IChat, IChatConnection } from "./chat";

export default {
  users: [
    { id: '1', name: 'Artem Oleinikov', phone: '+380937071741', email: 'art@email.com', createdAt: new Date().toString(), },
    { id: '2', name: 'Maria Onishenko', phone: '+380500553637', email: 'maru@email.com', createdAt: new Date().toString(), },
    { id: '3', name: 'Anna Borysenko', phone: '+380939393233', email: 'ann@email.com', createdAt: new Date().toString(), },
  ],
  messages: [
    { clientId: '1', text: 'Hello, Maria!', chatId: '1', createdAt: new Date().toString(), id: '1' },
    { clientId: '2', text: 'Hello, Artem! ', chatId: '1', createdAt: new Date().toString(), id: '1' },
    { clientId: '1', text: 'How are you?', chatId: '1', createdAt: new Date().toString(), id: '1' },
    { clientId: '2', text: 'I\'m good! How about you?', chatId: '1', createdAt: new Date().toString(), id: '1' },
    { clientId: '3', text: 'Hello gays! Nice to see you here!', chatId: '1', createdAt: new Date().toString(), id: '1' },
  ],
  chats: [
    { id: '1', title: 'Chat for friends', createdAt: new Date().toString() }
  ],
  chatsConnection: [
    {  id: "1", clientId: '1', chatId: '1', createdAt: new Date().toString(), },
    {  id: "2", clientId: '2', chatId: '1', createdAt: new Date().toString(), },
    {  id: "3", clientId: '3', chatId: '1', createdAt: new Date().toString(), },
  ],
} as {
  users: IClient[],
  messages: IMessage[],
  chats: IChat[],
  chatsConnection: IChatConnection[]
}
