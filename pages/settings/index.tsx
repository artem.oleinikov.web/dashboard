import { useAuth } from "../../hooks/useAuth";

import type { NextPage } from 'next'

const Settings: NextPage = () => {
  const auth = useAuth();
  return (
    <div >
      Settings
    </div>
  )
}

export default Settings
