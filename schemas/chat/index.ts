import { GraphQLID, GraphQLObjectType, GraphQLString, GraphQLList } from "graphql";
import { MessageType, IMessage } from "../message";

export interface IChat {
  id: string;
  title: string;
  messages: IMessage[];
  createdAt: string;
}

export interface IChatConnection {
  id: string;
  chatId: string;
  clientId: string;
  createdAt: string;
}

export const ChatFields = {
  id: { type: GraphQLID },
  title: { type: GraphQLString },
  messages: { type: new GraphQLList(MessageType) }
}

export const ChatType = new GraphQLObjectType<IChat>({
  name: 'Chat',
  fields: () => ChatFields
})

export const ChatConnectionType = new GraphQLObjectType<IChatConnection>({
  name: 'ChatConnection',
  fields: () => ({
    id: { type: GraphQLID },
    chatId: { type: GraphQLID },
    clientId: { type: GraphQLID },
  })
})
