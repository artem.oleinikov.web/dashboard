import { GraphQLID, Source } from "graphql";
import data from "../../fakeData";
import { MessageType } from "../";

interface Args {
  id: string;
}

const messageResolveArgs = {
  id: { type: GraphQLID, required: false, },
};

const messageResolve = (parent: Source, args: Args) => {
  return data.messages.filter(({ id } ) => id === args.id);
};

export const message = {
  type: MessageType,
  args: messageResolveArgs,
  resolve: messageResolve,
};
