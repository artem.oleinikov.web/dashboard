import { GraphQLObjectType, GraphQLString, Source, GraphQLNonNull } from "graphql";
import { IClient } from "..";

import { STATUS, STATUS_MUTATION } from "../../consts";
import { PrismaClient } from '@prisma/client'

const args = {
  name: { type: GraphQLNonNull(GraphQLString) },
  email: { type: GraphQLNonNull(GraphQLString) },
  phone: { type: GraphQLNonNull(GraphQLString) },
  password:{ type: GraphQLNonNull(GraphQLString) },
};

const resolve = async (parent: Source, args: Partial<IClient>): Promise<{ status: STATUS }> => {
  const prisma = new PrismaClient()

  const data: Omit<IClient, 'id' | 'createdAt'> = {
    name: args.name || '',
    email: args.email || '',
    password: args.password || '',
    phone: args.phone || '',
  };
  const user = await prisma.user.create({ data: { ...data, createdAt: new Date() } });
  console.log('[addClient] Client created', user);
  return {
    status: STATUS.success,
  }
};


export const addClientType = new GraphQLObjectType({
  name: 'AddClient',
  fields: () => ({
    status: { type: STATUS_MUTATION }
  })
})

export const addClient = {
  type: addClientType,
  args,
  resolve,
}
