import { GraphQLID, Source } from "graphql";
import data from "../../fakeData";
import { ChatType } from "../index";

interface Args {
  id?: string;
}

const clientResolveArgs = {
  id: { type: GraphQLID, required: true, },
};

const chatResolve = (parent: Source, args: Args) => {
  const chat = data.chats.find(({ id } ) => id === args.id);
  const chatMessages = data.messages.filter(({ chatId }) => chatId === args.id) || []

  const result = {
    ...chat,
    messages: chatMessages
  }

  return result;
};

export const chat = {
  type: ChatType,
  args: clientResolveArgs,
  resolve: chatResolve,
};
