import { GraphQLID, GraphQLObjectType, GraphQLString } from "graphql";

export interface IClient {
  id: string;
  name: string;
  email: string;
  phone: string;
  password?: string;
  createdAt: string;
}

export const ClientFields = {
  id: { type: GraphQLID },
  name: { type: GraphQLString },
  email: { type: GraphQLString },
  phone: { type: GraphQLString },
}

export const ClientType = new GraphQLObjectType<IClient>({
  name: 'Client',
  fields: () => ClientFields
})
