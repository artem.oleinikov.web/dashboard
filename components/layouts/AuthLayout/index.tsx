import { FC, ReactElement } from 'react';
import styles from "./layout.module.scss";

import { Navigation } from "../../Navigation";

export const AuthLayout: FC<{ children: ReactElement }> = ({ children }) => {
  return (
    <div>
      {children}
    </div>
  );
};
