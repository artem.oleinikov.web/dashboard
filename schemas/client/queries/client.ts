import { GraphQLID, Source } from "graphql";
import data from "../../fakeData";
import { ClientType } from "../";

interface Args {
  email?: string;
  id?: string;
}

const clientResolveArgs = {
  id: { type: GraphQLID, required: false, },
  email: { type: GraphQLID, required: false, },
};

const clientResolve = (parent: Source, args: Args) => {
  return data.users.find(({ id, email } ) => id === args.id || email === args.email);
};

export const client = {
  type: ClientType,
  args: clientResolveArgs,
  resolve: clientResolve,
};
