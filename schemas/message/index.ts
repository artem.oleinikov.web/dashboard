import { GraphQLID, GraphQLObjectType, GraphQLString } from "graphql";

export interface IMessage {
  id: string;
  clientId: string;
  text: string;
  chatId: string;
  createdAt: string;
}

export const MessageFields = {
  id: { type: GraphQLID },
  clientId: { type: GraphQLID },
  chatId: { type: GraphQLID },
  createdAt: { type: GraphQLString },
  text: { type: GraphQLString },
}

export const MessageType = new GraphQLObjectType<IMessage>({
  name: 'Message',
  fields: () => MessageFields
})
