import { GraphQLInt, GraphQLObjectType, Source, GraphQLList } from "graphql";

import { ClientType, IClient } from "../";

import data from "../../fakeData";

interface IClientsType {
  list: IClient[];
  current_page: number;
  pages: number;
}

export const ClientsType = new GraphQLObjectType<IClientsType>({
  name: 'Clients',
  fields: () => ({
    list: { type: new GraphQLList(ClientType) },
    current_page: { type: GraphQLInt },
    pages: { type: GraphQLInt },
  })
})

interface Args { limit?: number; page?: number }

const clientResolveArgs = {
  limit: { type: GraphQLInt, required: false },
  page: { type: GraphQLInt, required: false },
};


const clientsResolve = (parent: Source, args: Args) => {
  const { page = 1, limit = 10 } = args;

  const startIndex = (page - 1) * limit;
  const endIndex = startIndex + limit;

  const clients = data.users.slice(startIndex, endIndex);
  const pages = Math.round(data.users.length / limit);

  return {
    list: clients,
    current_page: page,
    pages,
  };
};

export const clients = {
  type: ClientsType,
  args: clientResolveArgs,
  resolve: clientsResolve,
};
