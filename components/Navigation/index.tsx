import styles from './navigation.module.scss';
import { useTranslation } from "react-i18next";
import { LINKS } from "../../contstants";
import Link from "next/link";

export const Navigation = () => {
  const { t } = useTranslation();
  return (
    <nav className={styles.navigation}>
      <ul>
        <Link href={LINKS.main.path}><li>{t(LINKS.main.label)}</li></Link>
        <Link href={LINKS.messages.path}><li>{t(LINKS.messages.label)}</li></Link>
        <Link href={LINKS.settings.path}><li>{t(LINKS.settings.label)}</li></Link>
      </ul>
    </nav>
  )
};
