import { useAuth } from "../hooks/useAuth";

import type { NextPage } from 'next'

const Home: NextPage = () => {
  const auth = useAuth();

  console.log('auth', auth);

  return (
    <div >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias aperiam
      asperiores at consequatur dignissimos, dolor dolore error et eum, illo, ipsam laborum optio recusandae reiciendis
      sed tempore vel veritatis!
    </div>
  )
}

export default Home
