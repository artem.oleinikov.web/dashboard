import { GraphQLObjectType, GraphQLSchema } from 'graphql';

// queries
import { client, clients } from "./client/queries";
import { chat, chats } from "./chat/queries";

// mutations
import { addClient } from "./client/mutations";

const rootQuery = new GraphQLObjectType({
  name: 'Query',
  fields: {
    chat,
    chats,
    client,
    clients,
  }
})


const rootMutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addClient
  }
})


export default new GraphQLSchema({
  query: rootQuery,
  mutation: rootMutation,
})
