// AUTH
export const SECRET_KEY = 'SECRET_KEY';
export const ACCESS_TOKEN_KEY = 'ACCESS_TOKEN';
export const REFRESH_TOKEN_KEY = 'REFRESH_TOKEN';

// Links
export const LINKS = {
  main: {
    path: '/',
    label: 'links.main',
  },
  messages: {
    path: '/chats',
    label: 'links.chats',
  },
  settings: {
    path: '/settings',
    label: 'links.settings',
  },
  login: {
    path: '/auth/login',
    label: 'links.login',
  },
  signup: {
    path: '/auth/signup',
    label: 'links.signup',
  },
  forgot_password: {
    path: '/auth/forgot_password',
    label: 'links.forgot_password',
  }
}
