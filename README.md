## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

## Update Database

First, run the development server:

```bash
prisma validate
```

if everything is valid, run the

```bash
 npx prisma db push          
```

if you have some errors, just try to fix it and run the ```prisma validate``` again.
