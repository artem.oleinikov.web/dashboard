import React, {
  FC,
  ReactElement,
  useState,
  useEffect,
  useContext,
  useCallback,
  useMemo,
} from "react";
import jwt from "jsonwebtoken";
import { useRouter } from "next/router";

import { AuthContext } from "./context";

import { JwtAccessPayload, Tokens, IAuthContext, LoginParams } from "./types";
import { ACCESS_TOKEN_KEY, LINKS, REFRESH_TOKEN_KEY, SECRET_KEY } from "../../contstants";

const useProvideAuth = (): IAuthContext => {
  const { push } = useRouter();
  const [user, setUser] = useState<null | JwtAccessPayload>(null);

  const _saveTokens = useCallback(({ accessToken, refreshToken }: Tokens) => {
    accessToken && localStorage.setItem(ACCESS_TOKEN_KEY, accessToken)
    refreshToken && localStorage.setItem(REFRESH_TOKEN_KEY, accessToken)
  }, []);

  const _getTokens = useCallback(() => ({
    accessToken: localStorage.getItem(ACCESS_TOKEN_KEY),
    refreshToken:localStorage.getItem(REFRESH_TOKEN_KEY)
  }), []);

  const _clearTokens = useCallback(() => {
    localStorage.removeItem(ACCESS_TOKEN_KEY)
    localStorage.removeItem(REFRESH_TOKEN_KEY)
  }, []);

  const login = useCallback((params: LoginParams) => {
    const accessToken = jwt.sign(params, SECRET_KEY);
    const refreshToken = jwt.sign(params, SECRET_KEY);

    setUser({ email: params.email, id: params.password });
    _saveTokens({ accessToken, refreshToken });
  }, []);

  const logout = useCallback(() => {
    setUser(null);
    _clearTokens();
  }, []);

  const checkToken = useCallback(() => {
    const { accessToken, refreshToken } = _getTokens();

    const decode = jwt.decode(accessToken || '');
    if (!decode) {
      _clearTokens();
      push(LINKS.login.path);
    } else {
      setUser(decode as JwtAccessPayload);
    }
  }, []);

  const isLogged = useMemo(() => Boolean(user), [user]);


  useEffect(() => {
    checkToken()
  }, []);

  return {
    login,
    logout,
    user,
    isLogged
  };
};

export const AuthProvider: FC<{ children: ReactElement }> = ({ children }) => {
  const auth = useProvideAuth()
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>
}

export const useAuth = () => useContext(AuthContext)
