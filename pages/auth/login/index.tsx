import * as React from 'react';
import NextLink from 'next/link';

import { useTranslation } from "react-i18next";

import {
  Avatar,
  Button,
  TextField,
  FormControlLabel,
  Checkbox,
  Link,
  Container,
  Box,
  Grid,
  Typography,
} from '@mui/material/';

import LockOutlinedIcon from '@mui/icons-material/LockOutlined';

import { LINKS } from "../../../contstants";
import { useAuth } from "../../../hooks/useAuth";
import { useEffect } from "react";
import { useRouter } from "next/router";


const LoginPage = () => {
  const { login, isLogged } = useAuth();
  const { t } = useTranslation();
  const { push } = useRouter();



  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);

    const email = String(data.get('email')) || '';
    const password = String(data.get('password')) || '';

    login({ email, password });
  };

  useEffect(() => {
    isLogged && push(LINKS.main.path)
  }, [isLogged])

  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item xs>
                <NextLink href={LINKS.forgot_password.path} passHref>
                  <Link variant="body2">
                    {t(LINKS.forgot_password.label)}
                  </Link>
                </NextLink>
              </Grid>
              <Grid item>
                <NextLink href={LINKS.signup.path} passHref>
                  <Link variant="body2">
                    {t(LINKS.signup.label)}
                  </Link>
                </NextLink>
              </Grid>
            </Grid>
          </Box>
        </Box>
    </Container>
  );
}

export default LoginPage
