import { GraphQLID, GraphQLList, GraphQLObjectType, Source } from "graphql";
import data from "../../fakeData";
import { ChatConnectionType, IChatConnection} from "../index";

interface Args {
  clientId?: string;
}

const chatsResolveArgs = {
  clientId: { type: GraphQLID, required: true, },
};

export const ChatsType = new GraphQLObjectType<IChatConnection[]>({
  name: 'Chats',
  fields: () => ({
    list: { type: new GraphQLList(ChatConnectionType) }
  })
})


const chatsResolve = (parent: Source, args: Args) => {
  const list = data.chatsConnection.filter(({ clientId } ) => clientId === args.clientId);
  return {
    list
  };
};

export const chats = {
  type: ChatsType,
  args: chatsResolveArgs,
  resolve: chatsResolve,
};
